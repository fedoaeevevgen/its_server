const mongoose = require('mongoose');
const projectSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=2&&text.length<=60;
            },
            message: 'Имя не прошло валидацию'
        }
    },
    email:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=5&&text.length<=60;
            },
            message: 'Email не прошёл валидацию'
        }
    },
    num:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=6&&text.length<=30;
            },
            message: 'Номер не прошёл валидацию'
        }
    },
    company:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=1&&text.length<=60;
            },
            message: 'Компания не прошла валидацию'
        }
    },
    date:{
        type:Date,
        default:new Date()
    },
    types:{
        type:Array,
        default:[0,0,0,0]
    },
    description:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=1&&text.length<=5000;
            },
            message: 'Описание не прошло валидацию'
        }
    },
    fileArray:{
        type:Array,
        default:[]
    }
},{
    collection: "projectSchema",
    versionKey: false
});



projectSchema.pre('save', function(next) {
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();
    next();
});

module.exports = mongoose.model('projectSchema', projectSchema);