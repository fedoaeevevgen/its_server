const mongoose = require('mongoose');
const crypto = require('crypto');
const studentSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=1&&text.length<=30;
            },
            message: 'Имя не прошло валидацию'
        }
    },
    lastName:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=1&&text.length<=30;
            },
            message: 'Фамилия не прошла валидацию'
        }
    },
    email:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=5&&text.length<=60;
            },
            message: 'Email не прошёл валидацию'
        }
    },
    num:{
        type:String,
        default:"",
        validate: {
            validator: function(text) {
                return text.length>=6&&text.length<=30;
            },
            message: 'Номер не прошёл валидацию'
        }
    },
    course:{
        type:String,
        default:"",
        validate: {
            validator: function(text) {
                return text.length>=1&&text.length<=30;
            },
            message: 'Курс не прошёл валидацию'
        }
    },
    experience:{
        type:String,
        default:"",
        validate: {
            validator: function(text) {
                return text.length>=0&&text.length<=1000;
            },
            message: 'Опыт не прошёл валидацию'
        }
    },
    slack:{
        type:String,
        default:""
    },
    level:{
        type:String,
        default:""
    },    
    characteristic:{
        type:String,
        default:""
    },
    type:{
        type:String,
        required:true,
        enum : ['frontend','backend', 'design','ios','android','ar','op','ai'],
    },
    status:{
        type:Boolean,
        default:false
    },
    date:{
        type:Date,
        default:new Date(),
    },
    salt:{
        type:String
    },
    hex:{
        type:String
    }
},{
    collection: "studentSchema",
    versionKey: false
});


studentSchema.virtual('email2')
    .set(function (email2) {
        this.salt = Math.random() + 'ahhhsdasd';
        this.hex = this.encryptEmail2(email2);
    });

    studentSchema.methods = {
        encryptEmail2: function (email2) {
            return crypto.createHmac('sha256', this.salt).update(email2).digest('hex');
        },
        checkEmail2: function (email2) {
            return this.encryptEmail2(email2) === this.hex;
        }
    };  

    studentSchema.statics = {
        checkEmail:async function (email) {
            const _email = await this.findOne({email: email}).exec();
            if (_email === null){
                return true;
            } else {
                return false;
            }
        }
    }  


studentSchema.pre('save', function(next) {
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();
    next();
});

module.exports = mongoose.model('studentSchema', studentSchema);