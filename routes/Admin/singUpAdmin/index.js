const Admin = require('../../../scheme/admin');
const log = require('../../../libs/logs')(module);
module.exports=async function (req, res){
    log.info("Попытка создания админа с IP: " + req.connection.remoteAddress + " (IP с учетом кодировки)");
    try{
    if(req.body.chek==='ITSそのSUSÞað er它的'){
        if(await Admin.checkName(req.body.name)){
            if(req.body.name!==undefined&&req.body.name.length>4&&req.body.name.length<=25){
                if(req.body.password!==undefined&&req.body.password.length>4&&req.body.password.length<=25){
                    var newAdmin = new Admin({
                        name: req.body.name,
                        password: req.body.password,
                    });
                    newAdmin.save(function(err , admin) {
                        if(err){
                            log.error(err);
                            res.send("0что-то не так");
                        }else{
                            log.info("Создание админа с IP: " + req.connection.remoteAddress + " (IP с учетом кодировки)");
                            res.send("1Админ успешно создан!");
                        }
                    }); 
                }else res.send("0Заполните поле пароль от 5 до 25 символов");
            }else res.send("0Заполните поле имя от 5 до 25 символов");
        }else res.send("0Данное имя уже занято");
    }else res.send("0Неверный пароль для создания!");
}catch(e){
    log.error(e)
    res.send("0Ошибка");
}
}
        