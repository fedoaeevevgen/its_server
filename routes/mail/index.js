const user = require ('./../../scheme/users')
const student = require ('./../../scheme/student')
const mail = require ('./../../libs/mail')
const config = require('./../../libs/config')
const log = require('./../../libs/logs')(module);
module.exports=function (req, res){
    log.info(req.body);
    try{ let type1;
    if(req.body.type!=="all")
     type1 = {type:req.body.type}
    else
     type1 = {}
    if(req.body.subject!==undefined&&req.body.text!==undefined&&req.body.type!==undefined&&req.body.set!==undefined){
        if(req.body.set==="subscriptions")
        user.find(type1,(err,result)=>{
            if(err||result===null||result.length===0){
                res.send("0Попробуй еще разок");  
            }else{
                let mas=[];
               // await result.map((el,index)=>{
                  for(let el in result)  mas.push(result[el].email);
                //})
                mail(config.email.ser,config.email.name,config.email.pas,req.body.subject,req.body.text,mas,function (sendErr) {
                    if(sendErr){
                        log.error(sendErr);
                        res.send("0Что-то пошло не так"); 
                    }else{
                        res.send("1По идеи сообщения отправились, но я не гарантирую :)");  
                    }
                })
            }
        })
        else
        student.find(type1,(err,result)=>{
            if(err||result===null||result.length===0){
                res.send("0Попробуй еще разок");  
            }else{
                let mas=[];
                // await result.map((el,index)=>{
                   for(let el2 in result) mas.push(result[el2].email);
                // })
                mail(config.email.ser,config.email.name,config.email.pas,req.body.subject,req.body.text,mas,function (sendErr1) {
                    if(sendErr1){
                        log.error(sendErr1);
                        res.send("0Что-то пошло не так"); 
                    }else{
                        res.send("1По идеи сообщения отправились, но я не гарантирую :)");  
                    }
                })
            }
        })
    }else{
        res.send("0Какое-то из полей не заполнено")
    }
}catch(e){
    log.error(e)
    res.send("0Ошибка");
}
}