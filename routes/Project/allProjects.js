const project = require('../../scheme/project');
const log = require('../../libs/logs')(module);

module.exports=function (req, res) {
  try{
    project.find().exec((err, result) => {
      if (err) {
          log.error(err);
          res.send ([]);
      }else
          if(result===null){
            res.send ([]);
          }else{
            res.send (result);
          }
    });
  }catch(e){
    log.error(e);
    res.send("0Ошибка");
}
}