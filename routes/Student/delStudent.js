const student = require('../../scheme/student');
const log = require('../../libs/logs')(module);

module.exports=function (req, res) {
    try{
    student.findByIdAndDelete(req.body._id,(err,result)=>{
      if(err||result===null){
          res.send("0Студент не найден");
      }else{
        result.mess = "Удаление студента"
      log.info(result);
          res.send("1Студент удален");
      }
  })
}catch(e){
    log.error(e)
    res.send("0Ошибка");
}
}