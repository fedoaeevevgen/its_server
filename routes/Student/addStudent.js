const student = require('../../scheme/student');
const send = require('../../libs/mailHTML');
const config = require('../../libs/config')
const log = require('../../libs/logs')(module);
module.exports = async function (req, res) {
    console.log(req.body)
    try {
        let chek = await student.checkEmail(req.body.email);
        if (chek) {
            var newStudent = new student(
                {...req.body, email2: req.body.email}
            );
            newStudent.save(function (err, user) {
                if (err) {
                    log.error(err);
                    res.status(500).send("Ошибка");
                } else {
                    send(config.email.ser, config.email.name, config.email.pas, req.body.type,
                        `<!doctype html>
                                <html lang="ru">
                                <head>
                                    <meta charset="UTF-8">
                                    <meta name="viewport" content="width=device-width"/>
                                </head>
                                
<body style="display: flex; justify-content: center">
<table width="100%">
    <tbody>
    <tr>
        <td>
            <table align="center">
                <tbody>
                                    <tr><td><a href="` + config.outIP + `" ><img src="` + config.outIP + `/files/endlogo.png"></a></td></tr>
                                    <tr>
                                        <td colspan="2"><img src="` + config.outIP + `/files/mg.jpg" width="580" height="225"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="font-size: 24px;
                                    color: #005FF9;
                                    font-weight: bold;
                                    padding-left: 30px;
                                    padding-top: 30px;
                                    padding-bottom: 15px;
                                ">Привет, ` + req.body.name + `
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"
                                                style=" padding-left: 30px;
                                                width: 450px;
                                                font-style: normal;
                                                font-weight: normal;
                                                line-height: 21px;
                                                font-size: 16px;">Ты решил записаться на обучение в IT Student? <span style="color: #005FF9">  </span>Тогда переходи по <a href="` + config.outIP + `/agreeITS/` + user.hex + `/` + user.email + `">ссылке</a> для подтверждения своей почты, и тогда с тобой свяжутся для обучения. Если данное сообщение пришло к тебе по ошибке, то переходи по этой <a href="` + config.outIP + `/disagreeITS/` + user.hex + `/` + user.email + `">ссылке.</a>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2" style="padding-top: 15px; padding-left: 30px; font-style: normal; color: #686868;
                                font-weight: normal;
                                line-height: 18px;
                                font-size: 14px;">Ждем тебя,</td></tr>
                                    <tr><td colspan="2" style="padding-left: 30px; font-style: normal; color: #686868;
                                font-weight: normal;
                                line-height: 18px;
                                font-size: 14px;">Команда IT Student</td></tr>
                                    <tr><td style="padding-left: 40%; padding-top: 30px; width: 35px"><a href="` + config.urlVK + `" ><img src="` + config.outIP + `/files/vk.png"></a></td><td style="padding-left: 15px; padding-top: 30px;"><a href="` + config.urlInst + `" ><img src="` + config.outIP + `/files/inst.png"></a></td></tr>
                                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>

</table>
</body>
                                </html>`,
                        //"Здравствуйте, "+req.body.name+"! Вы подписались на рассылку для обучения в its! Подтвердите свою почту, перейдя по следующей ссылке: "+config.outIP+"/agreeITS/"+user.hex+"/"+user.email+"\nЕсли это сообщение попало на вашу почту случайно, вы можете отказаться от рассылке, перейдя по следующей ссылке:\n"+config.outIP+"/disagreeITS/"+user.hex+"/"+user.email,
                        req.body.email, function (er) {
                            if (er) {
                                log.error(er);
                                res.status(500).send("Ошибка");
                            } else {
                                log.info("Отправленно сообщение на почту: " + req.body.email);
                                res.send("На вашу почту отправлено сообщение, для подтверждения.");
                            }
                        })
                }
            });
        } else
            res.status(412).send("Данная почта уже занята");
    } catch (e) {
        log.error(e)
        res.status(500).send("Ошибка");
    }
}