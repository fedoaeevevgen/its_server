﻿const adminChek = require('./../libs/adminChek');

module.exports = async function(app){  
    app.get('/api',require('./api'));

    app.post('/singUpAdmin', require('./Admin/singUpAdmin'));
    app.post('/singInAdmin', require('./Admin/singInAdmin'));
    app.post('/search',adminChek, require('./userOrSubscriber/searchUsersOrSubs'));
    app.post('/del',adminChek, require('./userOrSubscriber/delUserOrSub'));
    app.post('/mail',adminChek, require('./mail'));

    app.get('/disagree/:hex/:email', require('./userOrSubscriber/disagreeEmail'));
    app.get('/agree/:hex/:email', require('./userOrSubscriber/agreeEmail'));
    
    app.post('/add',require('./userOrSubscriber/addUserOrSub'));

    app.post("/addStudent" , require("./Student/addStudent"));//Добавление в талицу
    app.post("/students", adminChek, require("./Student/allStudents"));
    app.post("/editStudent", adminChek, require("./Student/editStudent"));
    app.post("/delStudent", adminChek, require("./Student/delStudent"));

    app.get('/agreeITS/:hex/:email', require('./Student/agreeEmail'));//Подтверждение
    app.get('/disagreeITS/:hex/:email', require('./Student/disagreeEmail'));//Отказ от обучения
    
    app.post("/addBrif", require("./Brif/addBrif"));
    app.post("/delBrif", adminChek, require("./Brif/delBrif"));

    app.post("/addProject", require("./Project/addProject"));
    app.post("/delProject", adminChek, require("./Project/delProject"));

    app.post("/allProject", adminChek, require("./Project/allProjects"));
    app.post("/allBrif", adminChek, require("./Brif/allBrifs"));

    app.post("/logView", adminChek, require("./logView"));
    app.post("/direction", adminChek, require("./direction/put"));
    app.get("/direction", require("./direction/get"));
};