const config = require('../../libs/config');
const log = require('./../../libs/logs')(module);
const fs = require('fs');

module.exports=function (req, res) {
    var box = JSON.parse(req.body.array);
    fs.writeFile(__dirname+"/data.json",JSON.stringify(box) , function(error){
        if(error) {
            log.error(error);
            res.send("0Ошибка"); 
        }
        else {
            box.mes = "Изменения в направлениях" ;
            log.info(box);
            res.send("1Изменения сохранены");
        }
    });
}