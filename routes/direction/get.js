const users = require('../../scheme/users');
const config = require('../../libs/config');
const log = require('./../../libs/logs')(module);
const fs = require('fs');

module.exports=function (req, res) {
    fs.readFile(__dirname+"/data.json", "utf8", 
    function(error,data){
        if(error) res.send("[]"); // если возникла ошибка
        else res.send(data); // выводим считанные данные
});
}