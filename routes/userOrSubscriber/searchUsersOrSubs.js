const users = require('../../scheme/users');
const log = require('../../libs/logs')(module);

module.exports=function (req, res) {
  try{
  if(req.body.type!==undefined){
    users.find({type:req.body.type,status:true},{salt:0,hex:0,status:0}).exec((err, result) => {
      if (err) {
          log.error(err)
          res.send ([])
      }else
          if(result===null){
            res.send ([])
          }else{
            res.send (result)
          }
    });
  }else{res.send([])}
}catch(e){
  log.error(e)
  res.send("0Ошибка");
}
}