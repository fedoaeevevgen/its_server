const users = require('../../scheme/users');
const config = require('../../libs/config');
const log = require('../../libs/logs')(module);

module.exports=function (req, res) {
  try{
  users.findOneAndDelete({email:req.params.email,hex:req.params.hex},(err, result) => {
    if (err) {
        log.error(err);
        res.send ("Похоже ссылка повреждена");
    }else
        if(result===null){
          res.send ("Похоже ссылка повреждена");
        }else{
          log.info("Отмена подписки с почты:" + req.params.email);
          res.redirect(config.outIP+"/#/message/2");
        }
  });
}catch(e){
  log.error(e);
  res.send("0Ошибка");
}
}