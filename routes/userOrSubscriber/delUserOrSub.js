const users = require('../../scheme/users');
const log = require('../../libs/logs')(module);

module.exports=function (req, res) {
    try{
  users.findByIdAndDelete(req.body.id,(err,result)=>{
      if(err||result===null){
          log.info("Попытка удалить несуществующего юзера");
          res.send("0Подписчик не найден");
      }else{
            result.mess = "Удаление подписчика"
          log.info(result);
          res.send("1Подписчик удален");
      }
  })
}catch(e){
    log.error(e)
    res.send("0Ошибка");
}
}