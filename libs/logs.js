var winston = require('winston');

 function logger(app) {
    var path = app.filename;
    return new winston.Logger({
        transports:[
            new (winston.transports.File)({
                name:"error.log",
                level:"error",
                filename: 'logs/error.log',
                label: path
            }),
            new (winston.transports.File)({
                name:'allLog.log',
                level:"info",
                filename: 'logs/allLog.log',
                label: path
            }),
        ]
    })
}

module.exports =logger;