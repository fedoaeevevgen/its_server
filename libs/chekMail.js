const users = require('./../scheme/users');
const log = require('./logs')(module);

module.exports=function(){
    let date = new Date();
    date = new Date(date.getFullYear(),date.getMonth(),date.getDate()-1)
    users.deleteMany({date:{$lt:date}},(err,result)=>{
        if(err){
            log.error(err);
        }else{
            log.info("Удалено почт из таблицы подписки: " + result.n);
        }
        return
    })
}