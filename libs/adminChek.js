const jwt = require('jsonwebtoken');
const config = require('./config');
const log = require('./logs')(module);
const admins = require('./../scheme/admin');
module.exports = function verify(req, res, next){
    try{
        jwt.verify(req.body.token, config.jwt.secretOrKey, function (err, decoded) {
            if (decoded !== undefined){
                admins.findOne({token:decoded.token},function (error, odmem){
                    if(error||odmem==null){
                        res.send('Вы не авторизированны');
                    }else{  
                        next()
                    }
                })
            } else  if (err) {
                res.send('Вы не авторизированны');
            }
        });
    } catch (err) {
        if (err.message === "Cannot read property 'refreshToken' of null"){
            res.send('Вы не авторизированны');
        } else {
            log.error(err);
            res.send(err.message);
        }
    }
}
